#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <locale.h>
#include <wchar.h>
#include <string.h>

// Constants for the sizes of various objects.
#define SIZE 15               //Board Size
#define NUMTILES 7			  //Num Of Tiles Of Each Player
#define TOTALTILES 102		  //Total Tiles In Th Bag

// Constants for the word orientation.
#define DOWN 'V'       //Vertical
#define ACROSS 'H'		//Horizontal

// Constants for the all the game squares.
#define TRIPLE_WORD '�'
#define DOUBLE_WORD '@'
#define TRIPLE_LETTER '%'
#define DOUBLE_LETTER '&'
#define REGULAR ' '
#define Center '#'
#define Joker '?'

//Enum
enum CaseType
{
	Normal, D_Word, T_Word, D_Letter, T_Letter
};

//Structs 
struct Tile
{
	char letter;
	int score;
};

struct Bag
{
	struct Tile tile[TOTALTILES];
	int quantity;
};

struct Case
{
	char letter;
	enum CaseType caseType;
};

struct Player
{
	char name[20];
	int score;
	struct Tile pioche[NUMTILES];
};

//Functions Declaration
void FillBoard();
void FillCasesInBoard(int x, int y, char c, enum CaseType caseType);
void PrintBoard();
void FillBag();
int GetScore(char c);
struct Tile* pickTile();
void copyTile(struct Tile* dest, const struct Tile* source);
void printTiles(struct Player player, int length);

//Global Variables
struct Case Board[SIZE][SIZE];
struct Bag Bag;



int main()
{
	setlocale(LC_CTYPE, ""); //Handling the special characters

	struct Player player1 = { .name = "Player1" };
	struct Player player2 = { .name = "Player2" };
	//struct Player player3 = { .name = "Player3" };
	//struct Player player4 = { .name = "Player4" };

	

	


	
	while (true) 
	{
		clrscr();
		Bag.quantity = TOTALTILES;
		FillBoard();
		PrintBoard();
		FillBag();
		// Get 7 tiles for your hand.
		for (int i = 0; i < NUMTILES; i++)
			copyTile(&player1.pioche[i], pickTile());



		// Print Player & Score
		printf(" %s : Score %d\n", player1.name, player1.score);
		// Print out the tiles and process a single case.
		printTiles(player1, NUMTILES);

		break;
	}

	







	system("PAUSE");
	return 0;
}


//Functions Implementation
void FillBoard()
{
	int x, y;
	//Fill The Board With Initial Values
	for (x = 0; x < SIZE; x++)
	{
		for (y = 0; y < SIZE; y++)
		{
			Board[x][y].letter = REGULAR;
			Board[x][y].caseType = Normal;
		}
	}

	//Fill The Triple Words Cases
	FillCasesInBoard(0, 0, TRIPLE_WORD, T_Word);
	FillCasesInBoard(0, SIZE / 2, TRIPLE_WORD, T_Word);
	FillCasesInBoard(SIZE / 2, 0, TRIPLE_WORD, T_Word);

	//Fill The Double Words Cases
	for (x = 1; x < 5; x++)
		FillCasesInBoard(x, x, DOUBLE_WORD, D_Word);

	//Fill The Middle Double Word
	Board[SIZE / 2][SIZE / 2].letter = Center;
	Board[SIZE / 2][SIZE / 2].caseType = Normal;

	//Fill The Triple Letter Cases
	FillCasesInBoard(1, SIZE / 3, TRIPLE_LETTER, T_Letter);
	FillCasesInBoard(SIZE / 3, 1, TRIPLE_LETTER, T_Letter);
	FillCasesInBoard(SIZE / 3, SIZE / 3, TRIPLE_LETTER, T_Letter);

	//Fill The Double Letter Cases
	FillCasesInBoard(0, SIZE / 5, DOUBLE_LETTER, D_Letter);
	FillCasesInBoard(SIZE / 5, 0, DOUBLE_LETTER, D_Letter);
	FillCasesInBoard(2, 2 * SIZE / 5, DOUBLE_LETTER, D_Letter);
	FillCasesInBoard(2 * SIZE / 5, 2, DOUBLE_LETTER, D_Letter);
	FillCasesInBoard(2 * SIZE / 5, 2 * SIZE / 5, DOUBLE_LETTER, D_Letter);
	FillCasesInBoard(SIZE / 5, SIZE / 2, DOUBLE_LETTER, D_Letter);
	FillCasesInBoard(SIZE / 2, SIZE / 5, DOUBLE_LETTER, D_Letter);
}

void FillCasesInBoard(int x, int y, char c, enum CaseType caseType)
{
	Board[x][y].letter = c;
	Board[x][y].caseType = caseType;
	Board[x][SIZE - 1 - y].letter = c;
	Board[x][SIZE - 1 - y].caseType = caseType;
	Board[SIZE - 1 - x][y].letter = c;
	Board[SIZE - 1 - x][y].caseType = caseType;
	Board[SIZE - 1 - x][SIZE - 1 - y].letter = c;
	Board[SIZE - 1 - x][SIZE - 1 - y].caseType = caseType;
}

//Fill The Bag With Tiles
void FillBag()
{
	int letterFreq[27] = { 9, 2, 2, 3, 15, 2, 2, 2, 8, 1, 1, 5, 3,6, 6, 2, 1, 6,  6, 6, 6, 2, 1, 1, 1, 1,2 };
	int letter, freq, count = 0;

	for (letter = 0; letter < 26; letter++) {
		// Put in the correct number of tiles with the current letter.
		for (freq = 0; freq < letterFreq[letter]; freq++) {
			Bag.tile[count].letter = (char)(letter + 'A');
			Bag.tile[count].score = GetScore(Bag.tile[count].letter);
			count++;
		}
	}

	// Manually fill in the Joker.
	Bag.tile[count].letter = Joker;
	Bag.tile[count].score = GetScore(Bag.tile[count].letter);
	Bag.tile[count + 1].letter = Joker;
	Bag.tile[count + 1].score = GetScore(Bag.tile[count].letter);
	Bag.quantity = TOTALTILES;
}

//Get The Score Of Given Character
int GetScore(char c)
{
	if (c == 'D' || c == 'G' || c == 'M')
		return 2;
	if (c == 'B' || c == 'C' || c == 'P')
		return 3;
	if (c == 'F' || c == 'H' || c == 'V')
		return 4;
	if (c == 'J' || c == 'Q')
		return 8;
	if (c == 'K' || c == 'Z' || c == 'Y' || c == 'X' || c == 'W')
		return 10;
	if (c == Joker)
		return 0;

	return 1;
}

// Returns a pointer to a tile randomly chosen from the bag
struct Tile* pickTile()
{
	// Get the random index into the bag.   
	int randnum = rand() % (Bag.quantity);

	// Allocate space for the tile to be returned.
	struct Tile* tile;
	tile = malloc(sizeof(struct Tile));

	// Set up retval to be the randomly chosen tile.
	copyTile(tile, &(Bag.tile[randnum]));

	// Copy over where retval used to be stored with the last element.
	copyTile(&(Bag.tile[randnum]), &(Bag.tile[Bag.quantity - 1]));

	// Adjust the number of tiles in the bag pointed to by b.
	Bag.quantity -= 1;
	return tile;
}

void copyTile(struct Tile* dest, const struct Tile* source)
{
	dest->letter = source->letter;
	dest->score = source->score;
}

void printTiles(struct Player player, int length)
{
	int i;
	printf("\t\t");
	for (i = 0; i < length - 1; i++)
		printf(" %c%d ", player.pioche[i].letter, player.pioche[i].score);

	printf("%c %d\n", player.pioche[i].letter, player.pioche[i].score);
}

void PrintBoard() {

	int x, y;

	char a[26] = { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' };

	printf(" %c |", ' ');
	for (y = 0; y < SIZE; y++)
		printf(" %c |", a[y]);
	printf("\n");
	for (y = 0; y < 15; y++)
		printf("---+");
	printf("---+\n");

	// Print each row.
	for (x = 1; x <= SIZE; x++) {

		// Print a row header for each row.
		printf("%2d |", x);

		// Print each element of the current row.
		for (y = 0; y < SIZE; y++)
		{
			printf(" %c |", Board[x - 1][y].letter);
		}

		// Go to the new line.
		printf("\n");
		for (y = 0; y < 15; y++)
			printf("---+");
		printf("---+\n");
	}

	printf("\n");
}